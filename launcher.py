import camelot


def parse_tables(pdf_name, folder_name, pages):
    tables = camelot.read_pdf(pdf_name, flavor='stream', pages=pages)
    print(tables)

    tables.export(folder_name+'/table.html', f='html', compress=False)


parse_tables('pillar-3-report-15-02-2019.pdf', "RBS", '12,13')
parse_tables('190805-pillar-3-disclosures-at-30-june-2019.pdf', "HSBC", '7,8,9')
